<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ind4');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Gj{ltdLo7z{p`1_Q2aWu!>JQbf[q`E*mZ]kU<7!otwgo (Zrh.^<uy$bsA^h^aBy');
define('SECURE_AUTH_KEY',  'u9L$;4V+|*QMEiHyh>(,0wLr3KKjs!{?CJ.QNc0)^{*2VZ&QhEQqefS>U!?{6`-&');
define('LOGGED_IN_KEY',    'A*$WGGfCB/ZgE6TbBpo+l)SU@/R8dMtZ@F }I-t1`yQ;0cYJ>KS0XcQV;H/MOadm');
define('NONCE_KEY',        'PKrjA6d//WNe3k24/7:-cl1rt*0l|v1r|:)wveze6(&/Njdevo*5+byu/|w{9`nN');
define('AUTH_SALT',        ',oa#*B{Yc~HHY;j27/3.oij;y3.3XS<6=$RMylrBn4v#VTl/L:vI>{c#2 Z]yNb;');
define('SECURE_AUTH_SALT', '$rnv@3y(/maww])V6;[1OL#3N}C]kWsV_Iuzz/`JR2iX^$L__8DM<Ii&b.T|QTJa');
define('LOGGED_IN_SALT',   'K>qf,<vSJ~!]qz<{|*L<lK{%1SL]]w}$e>=PYepo!jWP`zcSSWtzpa+-vm6t-T/=');
define('NONCE_SALT',       'e,f^v;L=RF.U3EpPaHtpsvqq] DjoTb@+>yr.o6D@70U}[$qtSX{jm-k|?u(Jl4y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
