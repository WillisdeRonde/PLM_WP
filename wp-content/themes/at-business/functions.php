<?php
/**
 * Main file
 *
 * @package AT_Business
 */

// Load starting file.
require_once trailingslashit( get_stylesheet_directory() ) . 'includes/start.php';
