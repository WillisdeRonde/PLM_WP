=== AT Business ===

Contributors: axlethemes
Requires PHP: 5.2
Requires at least: 4.7
Tested up to: 4.9.6
Stable tag: 1.0.0

== Description ==

AT Business is a child theme of Business Key theme. You can try theme demo here - https://demo.axlethemes.com/at-business/

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Credits ==

AT Business WordPress Theme, Copyright (C) 2018, Axle Themes
AT Business is distributed under the terms of the GNU GPL

AT Business WordPress Theme is child theme of Business Key WordPress Theme, Copyright (C) 2018, Axle Themes
Business Key WordPress Theme is distributed under the terms of the GNU GPL

== Changelog ==

= 1.0.0 - Jun 22 2018 =
* Initial release

== Images ==

* All images are licensed under [CC0](http://creativecommons.org/publicdomain/zero/1.0/legalcode.txt)

* https://www.pexels.com/photo/man-in-white-dress-shirt-926390/
* https://pixabay.com/en/paper-business-document-office-3224643/
